package com.app.request.microservice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MicroServiceImplTest {

	@Autowired
	MicroServiceImpl microServiceImpl;
	
	@Test
	void test() throws Exception {
		boolean is = microServiceImpl.isCustomerAvailable("Bhangarwale_00001");
		Assert.assertEquals(true, is);
	}

}
