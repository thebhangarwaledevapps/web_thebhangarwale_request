package com.app.request.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.request.entity.CartItem;
import com.app.request.entity.Item;
import com.app.request.entity.ItemMedia;

@RunWith(SpringRunner.class)
@DataJpaTest
class ItemMediaRepositoryTest {

	@Autowired
	ItemMediaRepository itemImageRepository;
	
	@Autowired
	ItemRepository itemRepository;
	
	@Autowired
	CartItemRepository cartItemRepository;
	
	@Test
	void test() {
		
		ArrayList<ItemMedia> itemMediaList = new ArrayList<>();
		itemMediaList.add(new ItemMedia("imagepath1", "image/*"));
		itemMediaList.add(new ItemMedia("imagepath1", "video/*"));
		
		Item item = itemRepository.save(new Item("Loha", 200,400,400, itemMediaList));
		
		
		ArrayList<ItemMedia> list = new ArrayList<>();
		list.add(item.getMedias().get(1));
		
		item.getMedias().removeAll(list);
		
		Item item1 = itemRepository.save(item);
		
		System.out.print(item1.getMedias());
		
		List list1 = itemImageRepository.findAll();
		
		System.out.println(list1);

		
		
	}
	
	
	/*
	 2022-01-14 09:46:40.115  WARN 5424 --- [           main] o.h.engine.jdbc.spi.SqlExceptionHelper   : SQL Error: 20000, SQLState: 23503
2022-01-14 09:46:40.117 ERROR 5424 --- [           main] o.h.engine.jdbc.spi.SqlExceptionHelper   : INSERT on table 'ITEM_MEDIA' caused a violation of foreign key constraint 'FKRPEXTQOBHRK32A19IASBEJGMN' for key (2).  The statement has been rolled back.
2022-01-14 09:46:40.221  WARN 5424 --- [           main] o.h.engine.jdbc.spi.SqlExceptionHelper   : SQL Warning Code: 10000, SQLState: 01J01
2022-01-14 09:46:40.222  WARN 5424 --- [           main] o.h.engine.jdbc.spi.SqlExceptionHelper   : Database 'memory:c41da05c-6c26-43d6-8f4c-4176a9c586ca' not created, connection made to existing database instead.
2022-01-14 09:46:40.225  INFO 5424 --- [           main] o.s.t.c.transaction.TransactionContext   : Rolled back transaction for test: [DefaultTestContext@5c42d2b7 testClass = ItemImageRepositoryTest, testInstance = com.app.request.repository.ItemImageRepositoryTest@68b6f0d6, testMethod = test@ItemImageRepositoryTest, testException = org.springframework.dao.DataIntegrityViolationException: could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement, mergedContextConfiguration = [MergedContextConfiguration@625abb97 testClass = ItemImageRepositoryTest, locations = '{}', classes = '{class com.app.request.RequestApplication}', contextInitializerClasses = '[]', activeProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTestContextBootstrapper=true}', contextCustomizers = set[[ImportsContextCustomizer@5b1f29fa key = [org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration, org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration, org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration, org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration, org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration, org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration, org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration, org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration, org.springframework.boot.autoconfigure.sql.init.SqlInitializationAutoConfiguration, org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration, org.springframework.boot.test.autoconfigure.jdbc.TestDatabaseAutoConfiguration, org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManagerAutoConfiguration]], org.springframework.boot.test.context.filter.ExcludeFilterContextCustomizer@2f1de2d6, org.springframework.boot.test.json.DuplicateJsonObjectContextCustomizerFactory$DuplicateJsonObjectContextCustomizer@3a0baae5, org.springframework.boot.test.mock.mockito.MockitoContextCustomizer@0, org.springframework.boot.test.autoconfigure.OverrideAutoConfigurationContextCustomizerFactory$DisableAutoConfigurationContextCustomizer@db57326, org.springframework.boot.test.autoconfigure.actuate.metrics.MetricsExportContextCustomizerFactory$DisableMetricExportContextCustomizer@1a6c1270, org.springframework.boot.test.autoconfigure.filter.TypeExcludeFiltersContextCustomizer@351584c0, org.springframework.boot.test.autoconfigure.properties.PropertyMappingContextCustomizer@9eee96f, org.springframework.boot.test.autoconfigure.web.servlet.WebDriverContextCustomizerFactory$Customizer@e54303, org.springframework.boot.test.context.SpringBootTestArgs@1, org.springframework.boot.test.context.SpringBootTestWebEnvironment@0], contextLoader = 'org.springframework.boot.test.context.SpringBootContextLoader', parent = [null]], attributes = map['org.springframework.test.context.event.ApplicationEventsTestExecutionListener.recordApplicationEvents' -> false]]
2022-01-14 09:46:40.427  INFO 5424 --- [ionShutdownHook] j.LocalContainerEntityManagerFactoryBean : Closing JPA EntityManagerFactory for persistence unit 'default'
2022-01-14 09:46:40.429  INFO 5424 --- [ionShutdownHook] .SchemaDropperImpl$DelayedDropActionImpl : HHH000477: Starting delayed evictData of schema as part of SessionFactory shut-down'

	 
	 */
	
	
	/*create table cart_item (item_id bigint not null, customer_id varchar(255) not null, primary key (item_id))
	create table item (item_id bigint not null, item_name varchar(255), item_quantity integer not null, primary key (item_id))
	create table item_media (item_id bigint not null, item_media_path varchar(255), mime_type varchar(255), primary key (item_id))
	alter table item_media add constraint FKrpextqobhrk32a19iasbejgmn foreign key (item_id) references item
	select cartitem0_.item_id as item_id1_1_0_, cartitem0_.customer_id as customer2_1_0_ from cart_item cartitem0_ where cartitem0_.item_id=?
	CartItem [itemId=1, customerId=1]*/
	
}
