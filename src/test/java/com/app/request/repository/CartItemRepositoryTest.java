package com.app.request.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.request.entity.CartItem;

@RunWith(SpringRunner.class)
@DataJpaTest
class CartItemRepositoryTest {

	@Autowired
	CartItemRepository cartItemRepository;
	
	@Test
	void test() {
		CartItem cartItem = cartItemRepository.save(new CartItem(1L, "1"));
		System.out.println(cartItem);
		
		Optional<ArrayList<CartItem>> caritems = cartItemRepository.findCartItemsByCustomerId("1");
		System.out.println(caritems);
		
		
		
	}

}
