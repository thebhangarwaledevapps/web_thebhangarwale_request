package com.app.request.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

class MultipartFileTest {

	@Test
	void test_image_extension() throws Exception {
		File file = new File("C:\\Users\\user\\Downloads\\ic_space.jpg");
		InputStream stream = new FileInputStream(file);
		MultipartFile multipartFileToSend = new MockMultipartFile("file", file.getName(), "image/*",
				stream);
		String extension = multipartFileToSend.getContentType();
		System.out.println(extension);
	}
	
	@Test
	void test_video_extension() throws Exception {
		File file = new File("C:\\Users\\user\\Downloads\\Goa Election_ क्या Mining industry में दोबारा आएगी जान _( BBC Duniya with Sarika) (BBC Hindi).mp4");
		InputStream stream = new FileInputStream(file);
		MultipartFile multipartFileToSend = new MockMultipartFile("file", file.getName(), "video/*",
				stream);
		String extension = multipartFileToSend.getContentType();
		System.out.println(extension);
	}

}
