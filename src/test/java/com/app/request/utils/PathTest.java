package com.app.request.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestParam;

class PathTest {

	@Test
	void test() throws IOException {
		String type="image";
		String file="test.jpeg";
		
		
		
		final byte[] pdfBytes = Files.readAllBytes(Paths.get("src/main/download/".concat(type).concat("/").concat(file)));
	    
		System.out.println(pdfBytes);
		
	}

}
