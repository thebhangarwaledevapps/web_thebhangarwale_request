package com.app.request.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import org.hibernate.collection.internal.PersistentBag;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.app.request.entity.ItemsResponse;
import com.app.request.entity.Response;
import com.app.request.microservice.IMicroService;
import com.app.request.repository.CartItemRepository;
import com.app.request.repository.ItemMediaRepository;
import com.app.request.repository.ItemRepository;
import com.app.request.repository.RequestRepository;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@SpringBootTest
class RequestServiceTest {
	
	@Autowired
	ItemMediaRepository itemImageRepository;

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	CartItemRepository cartItemRepository;

	@Autowired
	RequestRepository requestRepository;
	
	@MockBean
	IMicroService microServiceImpl;
	
	@Autowired
	com.app.request.utils.Service utilService;
	
	@Autowired
	RequestService requestService;
	

	@Before
	void init() {
		microServiceImpl = mock(IMicroService.class);
	}
	
	
	@Test
	void test() throws Exception {
		/*final String customerId = "Bhangarwale_00001";
		when(microServiceImpl.isCustomerAvailable(customerId)).thenReturn(true);
		
		
		File file1 = new File("C:\\Users\\user\\Downloads\\WhatsApp Image 2022-01-04 at 4.45.31 PM.jpeg");
		InputStream stream1 = new FileInputStream(file1);
		MultipartFile multipartFileToSend1 = new MockMultipartFile("file", file1.getName(), "image/jpeg",
				stream1);
		
		
		File file = new File("C:\\Users\\user\\Downloads\\Goa Election_ क्या Mining industry में दोबारा आएगी जान _( BBC Duniya with Sarika) (BBC Hindi).mp4");
		InputStream stream = new FileInputStream(file);
		MultipartFile multipartFileToSend = new MockMultipartFile("file", file.getName(), "video/mp4",
				stream);
		
		
		MultipartFile[] multipartFiles = {multipartFileToSend1,multipartFileToSend};
		
		Response response = requestService.addItemToCart(customerId, "Loha", 400, multipartFiles);
		
		ItemsResponse item = requestService.getItemsInCart(customerId);
		
		System.out.println("Gson : "+item);*/
		
		
	}

}
