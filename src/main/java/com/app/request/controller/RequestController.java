package com.app.request.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.app.request.entity.CartItemResponse;
import com.app.request.entity.DeleteItemRequest;
import com.app.request.entity.DeleteMediaRequest;
import com.app.request.entity.Item;
import com.app.request.entity.ItemsResponse;
import com.app.request.entity.MediasResponse;
import com.app.request.entity.RequestResponse;
import com.app.request.entity.RequestStatus;
import com.app.request.entity.RequestStatusResponse;
import com.app.request.entity.Response;
import com.app.request.exception.CartItemOptionalNullException;
import com.app.request.exception.CartItemsEmptyException;
import com.app.request.exception.CartItemsNullException;
import com.app.request.exception.ImageOutOfRangeException;
import com.app.request.exception.InvalidAddressIdException;
import com.app.request.exception.InvalidCustomerIdException;
import com.app.request.exception.InvalidImageIdException;
import com.app.request.exception.InvalidItemIdException;
import com.app.request.exception.InvalidItemQuantitySelectException;
import com.app.request.exception.InvalidItemSelectException;
import com.app.request.exception.InvalidRequestIdException;
import com.app.request.exception.InvalidRequestStatusException;
import com.app.request.exception.ItemListNullException;
import com.app.request.exception.ItemNotUpdatedException;
import com.app.request.exception.ItemNullException;
import com.app.request.exception.ItemOptionalException;
import com.app.request.exception.ItemOptionalIsNotPresentException;
import com.app.request.exception.IterableNullException;
import com.app.request.exception.NoItemsSelectedForRemoveException;
import com.app.request.exception.NoMediasSelectedForRemoveException;
import com.app.request.exception.RequestItemsEmptyException;
import com.app.request.exception.RequestItemsNullException;
import com.app.request.exception.RequestNullException;
import com.app.request.exception.RollbackException;
import com.app.request.exception.SaveRequestException;
import com.app.request.exception.TimeNullException;
import com.app.request.exception.UpdateItemQuantityException;
import com.app.request.service.RequestService;
import com.app.request.utils.MessageUtils;

@RestController
@RequestMapping("/request")
public class RequestController {

	@Autowired
	private RequestService requestService;
		
	@RequestMapping(value = "/addItemToCart", method = RequestMethod.POST)
	public ResponseEntity<Response> addItemToCart(
			@RequestParam("customerId") String customerId,
			@RequestParam("bhangarType") String bhangarType,
			@RequestParam("bhangarUnit") String bhangarUnit,
			@RequestParam("bhangarPrice") Double bhangarPrice,
			@RequestParam("itemQuantityByUser") int itemQuantityByUser,
			@RequestParam(name ="media",required = false) MultipartFile[] mediaList
			) {
		Response response;
		ResponseEntity<Response> responseEntity = null;
		try {
			response = requestService.addItemToCart(customerId,bhangarType,bhangarUnit,bhangarPrice,itemQuantityByUser,mediaList);
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);		
		} catch (InvalidItemSelectException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidItemQuantitySelectException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			response = new Response(MessageUtils.SOMETHING_WENT_WRONG) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		return responseEntity;			
	}
	
	@RequestMapping(value = "/getItemsInCart", method = RequestMethod.GET)
	public ResponseEntity<ItemsResponse> getItemsInCart(
			@RequestParam("customerId") String customerId
			) {
		ItemsResponse response;
		ResponseEntity<ItemsResponse> responseEntity = null;
		try {
			response = requestService.getItemsInCart(customerId);
			responseEntity = new ResponseEntity<ItemsResponse>(response, HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			response = new ItemsResponse(e.getMessage());
			responseEntity = new ResponseEntity<ItemsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);		
		} catch (CartItemsEmptyException e) {
			response = new ItemsResponse(e.getMessage());
			responseEntity = new ResponseEntity<ItemsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			response = new ItemsResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<ItemsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		return responseEntity;
	}
	
	
	@RequestMapping(value = "/updateItemToCart", method = RequestMethod.POST)
	public ResponseEntity<Response> updateItemToCart(
			@RequestParam("customerId") String customerId,
			@RequestParam("itemId") Long itemId,
			@RequestParam(name = "itemQuantity",required = false) Integer itemQuantity,
			@RequestParam(name ="media",required = false) MultipartFile[] mediaList) {
		Response response;
		ResponseEntity<Response> responseEntity = null;
		try { 
			response = requestService.updateItemInCart(customerId,itemId,itemQuantity,mediaList);
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.OK);
		} catch (InvalidItemQuantitySelectException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);		
		} catch (ImageOutOfRangeException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);		
		} catch (Exception e) {
			response = new Response(MessageUtils.SOMETHING_WENT_WRONG) {};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);		
		}
		return responseEntity;		
	}
		
	/*@RequestMapping(value = "/createOrder", method = RequestMethod.GET)
	public ResponseEntity<Response> createOrder(
			@RequestParam("customerId") Long customerId,
			@RequestParam("addressId") Long addressId) {
		
		Response response;
		ResponseEntity<Response> responseEntity = null;
		
		try {
			response = requestService.createOrder(customerId, addressId);
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (InvalidAddressIdException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (CartItemOptionalNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (CartItemOptionalIsNotPresentException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (CartItemsNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (CartItemsEmptyException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (RequestItemsNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (RequestItemsEmptyException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (DateNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (TimeNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (SaveRequestException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			response = new Response(MessageUtils.SOMETHING_WENT_WRONG) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			return responseEntity;
		}
	}
	
	
	@RequestMapping(value = "/getRequests", method = RequestMethod.GET)
	public ResponseEntity<RequestResponse> getRequests(@RequestParam("customerId") Long customerId) {
		
		RequestResponse requestResponse;
		ResponseEntity<RequestResponse> responseEntity = null;
		
		try {
			requestResponse = requestService.getRequests(customerId);
			responseEntity = new ResponseEntity<RequestResponse>(requestResponse,HttpStatus.OK);
		}catch (InvalidCustomerIdException e) {
			requestResponse = new RequestResponse(e.getMessage());
			responseEntity = new ResponseEntity<RequestResponse>(requestResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (IterableNullException e) {
			requestResponse = new RequestResponse(e.getMessage());
			responseEntity = new ResponseEntity<RequestResponse>(requestResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (RequestNullException e) {
			requestResponse = new RequestResponse(e.getMessage());
			responseEntity = new ResponseEntity<RequestResponse>(requestResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			requestResponse = new RequestResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<RequestResponse>(requestResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			return responseEntity;
		}
	}
	
	@RequestMapping(value = "/cancelRequest", method = RequestMethod.GET)
	public ResponseEntity<Response> cancelRequest(
			@RequestParam("customerId") Long customerId,
			@RequestParam("requestId") Long requestId) {
		
		Response response;
		ResponseEntity<Response> responseEntity = null;
		
		try {
			response = requestService.cancelRequest(customerId, requestId);
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.OK);
		} catch (InvalidRequestIdException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleOptionalRequestNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestStatusException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestObjectIsNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNullException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNotPresentException e) {
			response = new Response(e.getMessage()) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			response = new Response(MessageUtils.SOMETHING_WENT_WRONG) {};
			responseEntity = new ResponseEntity<Response>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			return responseEntity;
		}
	} 
	
	
	@RequestMapping(value = "/confirmRequestByAdmin", method = RequestMethod.GET)
	public ResponseEntity<RequestStatusResponse> confirmRequestByAdmin(
			@RequestParam("customerId") Long customerId,
			@RequestParam("requestId") Long requestId) {
		
		RequestStatusResponse requestStatusResponse;
		ResponseEntity<RequestStatusResponse> responseEntity = null;
		try {
			requestStatusResponse = requestService.confirmRequestByAdmin(customerId, requestId);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestIdException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleOptionalRequestNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestStatusException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestObjectIsNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNotPresentException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			requestStatusResponse = new RequestStatusResponse(MessageUtils.SOMETHING_WENT_WRONG, null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			return responseEntity;
		}
		
		
	}
	
	
	@RequestMapping(value = "/cancelRequestByAdmin", method = RequestMethod.POST)
	public ResponseEntity<RequestStatusResponse> cancelRequestByAdmin(
			@RequestParam("customerId") Long customerId,
			@RequestParam("requestId") Long requestId) {
		
		RequestStatusResponse requestStatusResponse;
		ResponseEntity<RequestStatusResponse> responseEntity = null;
		
		
		try {
			requestStatusResponse = requestService.cancelRequestByAdmin(customerId, requestId);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestIdException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleOptionalRequestNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestStatusException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestObjectIsNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNotPresentException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			requestStatusResponse = new RequestStatusResponse(MessageUtils.SOMETHING_WENT_WRONG, null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			return responseEntity;
		}
		
		
		
	}
	
	
	@RequestMapping(value = "/completeRequestByAdmin", method = RequestMethod.POST)
	public ResponseEntity<RequestStatusResponse> completeRequestByAdmin(
			@RequestParam("customerId") Long customerId,
			@RequestParam("requestId") Long requestId) {
		
		RequestStatusResponse requestStatusResponse;
		ResponseEntity<RequestStatusResponse> responseEntity = null;
		
		
		try {
			requestStatusResponse = requestService.completeRequestByAdmin(customerId, requestId);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestIdException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleOptionalRequestNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidRequestStatusException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestObjectIsNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNullException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (BhangarwaleRequestOptionalIsNotPresentException e) {
			requestStatusResponse = new RequestStatusResponse(e.getMessage(), null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			requestStatusResponse = new RequestStatusResponse(MessageUtils.SOMETHING_WENT_WRONG, null);
			responseEntity = new ResponseEntity<RequestStatusResponse>(requestStatusResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			return responseEntity;
		}
	}*/
	
	
	@RequestMapping(value = "/deleteItemsFromCart", method = RequestMethod.POST)
	public ResponseEntity<ItemsResponse> deleteItemsFromCart(@RequestBody DeleteItemRequest deleteItemRequest) {
		ItemsResponse cartItemResponse;
		ResponseEntity<ItemsResponse> responseEntity = null;
		try {
			cartItemResponse = requestService.deleteItemInCart(deleteItemRequest.getCustomerId(), deleteItemRequest.getItems());
			responseEntity = new ResponseEntity<ItemsResponse>(cartItemResponse,HttpStatus.OK);
		} catch (InvalidCustomerIdException e) {
			cartItemResponse = new ItemsResponse(e.getMessage());
			responseEntity = new ResponseEntity<ItemsResponse>(cartItemResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoItemsSelectedForRemoveException e) {
			cartItemResponse = new ItemsResponse(e.getMessage());
			responseEntity = new ResponseEntity<ItemsResponse>(cartItemResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			cartItemResponse = new ItemsResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<ItemsResponse>(cartItemResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
		
	@RequestMapping(value = "/src/main/download", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getMedia(@RequestParam String type,@RequestParam String file){		
		ResponseEntity<byte[]> responseEntity = null;
		try {
			final byte[] pdfBytes = Files.readAllBytes(Paths.get("src/main/download/".concat(type).concat("/").concat(file)));
			final HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.parseMediaType(new MimetypesFileTypeMap().getContentType(new File(file))));
		    headers.setContentDispositionFormData("attachment", null);
		    headers.setCacheControl("no-cache");
		    responseEntity = new ResponseEntity<>(pdfBytes, headers, HttpStatus.OK);
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;		
	}
	
	@RequestMapping(value = "/deleteImagesFromItem", method = RequestMethod.POST)
	public ResponseEntity<MediasResponse> deleteImagesFromItem(@RequestBody DeleteMediaRequest deleteMediaRequest) {
		MediasResponse mediasResponse;
		ResponseEntity<MediasResponse> responseEntity = null;
		try {
			mediasResponse = requestService.deleteImagesFromItem(deleteMediaRequest.getCustomerId(), deleteMediaRequest.getItemId() ,deleteMediaRequest.getItemMedias());
			responseEntity = new ResponseEntity<MediasResponse>(mediasResponse,HttpStatus.OK);
		} catch (NoMediasSelectedForRemoveException e) {
			mediasResponse = new MediasResponse(e.getMessage());
			responseEntity = new ResponseEntity<MediasResponse>(mediasResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			mediasResponse = new MediasResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<MediasResponse>(mediasResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}	
	
}