package com.app.request.utils;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Profile;
import org.springframework.web.multipart.MultipartFile;
import com.app.request.entity.ItemMedia;
import com.app.request.exception.ImageOutOfRangeException;

@org.springframework.stereotype.Service
@Profile("dev")
public class DevelopmentUtilServiceImpl implements IUtilService{

	@Override
	public ArrayList<ItemMedia> saveMedia(MultipartFile[] files) throws ImageOutOfRangeException {
		return (ArrayList<ItemMedia>) Arrays.asList(files).stream().map(new Function<MultipartFile, ItemMedia>() {
			@Override
			public ItemMedia apply(MultipartFile multipartFile) {
				ItemMedia itemMedia = null;
				try {
					final String fileName = getFileName(multipartFile);
					multipartFile.transferTo(Paths.get(fileName));
					final File savedFile = new File(fileName);
					if (savedFile.exists()) {
						itemMedia = new ItemMedia(getPath(multipartFile, savedFile.getName()), multipartFile.getContentType());
					}
				} catch (Exception e) {}
				return itemMedia;
			}
		})
		.filter(ItemImage -> ItemImage != null).collect(Collectors.toList());
	}
	
	private String getFileName(MultipartFile multipartFile) {
		if (multipartFile.getContentType().startsWith("image")) {
			final String IMAGE_PATH = "src\\main\\download\\image\\";
			final String IMAGE_TYPE = ".webp";
			return IMAGE_PATH + "Bhangarwale_" + new Random().nextInt() + IMAGE_TYPE;
		} else if (multipartFile.getContentType().startsWith("video")) {
			final String VIDEO_PATH = "src\\main\\download\\video\\";
			final String VIDEO_TYPE = ".mp4";
			return VIDEO_PATH + "Bhangarwale_" + new Random().nextInt() + VIDEO_TYPE;
		}
		return null;
	}

	private String getPath(MultipartFile multipartFile, String fileName) {
		if (multipartFile.getContentType().startsWith("image")) {
			return "/src/main/download?type=image&file=".concat(fileName);
		} else if (multipartFile.getContentType().startsWith("video")) {
			return "/src/main/download?type=video&file=".concat(fileName);
		}
		return null;
	}

	
}
