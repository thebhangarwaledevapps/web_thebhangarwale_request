package com.app.request.utils;

public class MessageUtils {
	
	public static final int MINQUANTITY = 300;
	public static final String SOMETHING_WENT_WRONG = "Something went wrong";
	public static final String PLEASE_SELECT_VALID_ITEM = "Please select valid item";
	public static final String VALID_ITEM_QUANTITY = "Quanity should be greater than\t"+MINQUANTITY+"\tkg";
	public static final String ITEM_SAVE_SUCCESSFULLY = "Item saved successfully";
	public static final String ITEM_UPDATE_SUCCESSFULLY = "Item updated successfully";
	public static final String NO_ITEMS_FOUND = "No items found";
	public static final String ITEM_REMOVE_SUCCESSFULLY = "Item removed successfully";
	public static final String PLEASE_ADD_ATLEAST_ONE_ITEM = "Please add atleast 1 item in cart";
	public static final String REQUEST_CREATED_SUCCESSFULLY = "Request created successfully";
	public static final String NO_REQUEST_FOUND = "No requests found";
	public static final String MAIL_NOT_SEND_SUCCESSFULLY = "Your order has placed but we fail to notify our admin so we request you to make us one phone call";
	public static final String INVALID_CUSTOMER_ID = "Please enter valid customer id";
	public static final String INVALID_REQUEST_ID = "Please enter valid request id";
	public static final String REQUEST_STATUS_UPDATED_SUCCESSFULLY = "Request status updated successfully";
	public static final String INVALID_REQUEST_STATUS = "Please Enter Request status";
	public static final String REQUEST_CONFIRMED_SUCCESSFULLY = "Request confirmed successfully";
	public static final String REQUEST_CANCELLED_SUCCESSFULLY = "Request cancelled successfully";
	public static final String REQUEST_COMPLETED_SUCCESSFULLY = "Request completed successfully";
	public static final String ORDER_CREATED_SUCCESSFULLY = "Request created successfully";
	public static final String NO_ITEMS_SELECTED = "No items selected";
	public static final String VALID_QUANTITY = "Please enter valid quantity";
	public static final String NO_MEDIAS_SELECTED = "No medias selected";
	public static final String MEDIA_REMOVE_SUCCESSFULLY = "Media removed successfully";
	public static final String VALID_IMAGE_QUANTITY = "Can't store more than 3 images";
	
	
	
	
	
	
	
}
