package com.app.request.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import com.app.request.entity.Image;
import com.app.request.entity.Item;
import com.app.request.entity.ItemMedia;
import com.app.request.entity.Media;
import com.app.request.entity.Video;
import com.app.request.exception.ImageOutOfRangeException;

@org.springframework.stereotype.Service
public class UtilService implements Service {

	@Autowired
	IUtilService iUtilService;
	
	@Value("${facebook.client_id}")
	private String client_id;

	@Value("${facebook.client_secret}")
	private String client_secret;

	@Value("${facebook.page_id}")
	private String page_id;

	@Value("${email.from}")
	private String from;

	@Value("${email.password}")
	private String password;

	@Value("${email.to}")
	private String to;

	@Override
	public ArrayList<ItemMedia> saveMedia(MultipartFile[] files) throws ImageOutOfRangeException {
		if (files == null) {
			return null;
		}
		if (files.length == 0) {
			return null;
		}
		if (files.length > 3) {
			throw new ImageOutOfRangeException();
		}		
		return iUtilService.saveMedia(files);		
	}

	@Override
	public String generateHTMLForOrderRequest(String requestNumber, String customerId, String customerName,
			String customerPhoneNumber, double latLocation, double longLocation, String address, String date,
			ArrayList<Item> items) {
		try {
			ClassPathResource res = new ClassPathResource("index.html");
			final Document doc = Jsoup.parse(res.getFile(), "UTF-8");
			if (requestNumber != null) {
				doc.getElementById("row_request_value").text(requestNumber);
			} else {
				doc.getElementById("row_request_value").text("");
			}
			if (customerId != null) {
				doc.getElementById("row_customer_id_value").text(customerId);
			} else {
				doc.getElementById("row_customer_id_value").text("");
			}
			if (customerName != null) {
				doc.getElementById("row_name_value").text(customerName);
			} else {
				doc.getElementById("row_name_value").text("");
			}
			if (customerPhoneNumber != null) {
				doc.getElementById("href_phone_number_value").attr("href", "tel:" + customerPhoneNumber)
						.text(customerPhoneNumber);
			} else {
				doc.getElementById("href_phone_number_value").text("");
			}
			if (address != null) {
				doc.getElementById("href_address_value")
						.attr("href", "http://www.google.com/maps/place/" + latLocation + "," + longLocation)
						.text(address);
			} else {
				doc.getElementById("href_address_value").text("");
			}
			if (date != null) {
				doc.getElementById("row_date_value").text(date);
			} else {
				doc.getElementById("row_date_value").text("");
			}
			if (items != null && items.size() > 0) {
				items.forEach(new Consumer<Item>() {
					@Override
					public void accept(Item item) {
						final Element element = doc.select("tbody").last().appendElement("tr").attr("bgcolor",
								"#f5f5f0");
						element.appendChild(new Element("td").text(item.getItemName()));
						element.appendChild(new Element("td").text(String.valueOf(item.getItemQuantity())));
						item.getMedias().forEach(new Consumer<ItemMedia>() {
							@Override
							public void accept(ItemMedia media) {
								Element imageCell = new Element("td");
								Element aHrefLink = new Element("a");
								aHrefLink.text(media.getItemMediaPath()
										.substring(media.getItemMediaPath().lastIndexOf('/') + 1));
								aHrefLink.attr("href", media.getItemMediaPath());
								aHrefLink.attr("target", "_blank");
								imageCell.appendChild(aHrefLink);
								element.appendChild(imageCell);
							}
						});
					}
				});
			}
			return doc.html();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void sendEmail(String requestNumber, String customerId, String customerName, String customerPhoneNumber,
			double latLocation, double longLocation, String address, String date, ArrayList<Item> items) {
		try {
			final String htmlBody = generateHTMLForOrderRequest(requestNumber, customerId, customerName,
					customerPhoneNumber, latLocation, longLocation, address, date, items);

			final String subject = "Bhangarwale New Request";

			final Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			final Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});

			final MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);

			final MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(htmlBody, "text/html");

			final Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);
			message.setContent(multipart);

			Transport.send(message);

		} catch (Exception e) {

		}
	}

	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		String strDate = formatter.format(date);
		return strDate;
	}

	public static String getCurrentTime() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
		String strDate = formatter.format(date);
		return strDate;
	}

	

}
