package com.app.request.utils;

import java.util.ArrayList;

import org.springframework.web.multipart.MultipartFile;

import com.app.request.entity.Item;
import com.app.request.entity.ItemMedia;
import com.app.request.exception.ImageOutOfRangeException;

@org.springframework.stereotype.Service
public interface Service {

	public ArrayList<ItemMedia> saveMedia(MultipartFile[] files) throws ImageOutOfRangeException;

	public String generateHTMLForOrderRequest(String requestNumber, String customerId, String customerName, String customerPhoneNumber,
			double latLocation, double longLocation, String address, String date, ArrayList<Item> items);
	
	public void sendEmail(String requestNumber, String customerId, String customerName, String customerPhoneNumber,
			double latLocation, double longLocation, String address, String date, ArrayList<Item> items);
	
}