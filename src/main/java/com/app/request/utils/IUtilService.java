package com.app.request.utils;

import java.util.ArrayList;
import org.springframework.web.multipart.MultipartFile;
import com.app.request.entity.ItemMedia;
import com.app.request.exception.ImageOutOfRangeException;

@org.springframework.stereotype.Service
public interface IUtilService {
	
	public ArrayList<ItemMedia> saveMedia(MultipartFile[] files) throws ImageOutOfRangeException;
	
}
