package com.app.request.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.request.entity.CustomerDeveloperResponse;
import com.app.request.entity.CustomerOrderRequest;
import com.app.request.entity.CustomerProfileDeveloperResponse;
import com.app.request.entity.Item;
import com.app.request.exception.CartItemOptionalNullException;
import com.app.request.exception.CartItemsNullException;
import com.app.request.repository.CartItemRepository;
import com.app.request.repository.ItemRepository;
import com.app.request.entity.AddressForDeveloperRequest;
import com.app.request.entity.AddressForDeveloperResponse;
import com.app.request.entity.BhangarwaleRequest;
import com.app.request.entity.CartItem;
import com.app.request.entity.CartItemResponse;

/*@Service
public class SendEmailService implements Func<BhangarwaleRequest, Void>{


	@Autowired
	com.app.request.utils.Service emailService;
	
	@Autowired
	com.app.request.utils.Service customerService;
	
	@Autowired
	com.app.request.utils.Service addressService;
	
	@Autowired
	CartItemRepository cartItemRepository;

	@Autowired
	ItemRepository itemRepository;
	
	private Long requestNumber = null;
	private Long customerId = null;
	private String date = null;
	private String customerName = null;
	private String phoneNumber = null;
	private Long addressId = null;
	private double latLocation = 0;
	private double longLocation = 0;
	private String address = null;
	private ArrayList<Item> items = null;
		
	@Override
	public Void apply(BhangarwaleRequest input) throws Exception {
		if(input.getRequestId()!=null) {
			requestNumber = input.getRequestId();
		}
		if(input.getCustomerId()!=null) {
			customerId = input.getCustomerId();	
		}
		try {
			CustomerDeveloperResponse customerDeveloperResponse = (CustomerDeveloperResponse) customerService.get(customerId);
			customerName = customerDeveloperResponse.getCustomer().getName();
			phoneNumber = customerDeveloperResponse.getCustomer().getPhoneNumber();
		}catch (Exception e) {}
		try {
			if(input.getAddressId()!=null) {
				addressId = input.getAddressId();
			}
			AddressForDeveloperRequest addressForDeveloperRequest = new AddressForDeveloperRequest(customerId, addressId);
			AddressForDeveloperResponse addressForDeveloperResponse = (AddressForDeveloperResponse) addressService.get(addressForDeveloperRequest);
			latLocation = addressForDeveloperResponse.getAddress().getLatLocation();
			longLocation = addressForDeveloperResponse.getAddress().getLongLocation();
			address = addressForDeveloperResponse.getAddress().getAddress();
		}catch (Exception e) {}
		if(input.getCreatedDate()!=null) {
			date = input.getCreatedDate();
		}
		Optional<ArrayList<CartItem>> cartItemsOptional = cartItemRepository.findCartItemsByCustomerId(customerId);
		if(cartItemsOptional!=null && cartItemsOptional.isPresent()) {
			ArrayList<CartItem> cartItems = cartItemsOptional.get();	
			List<Long> itemIds = cartItems.stream().map(cartItem-> cartItem.getItemId()).collect(Collectors.toList());
			Iterable<Item> iterable = itemRepository.findAllById(itemIds);
			items = (ArrayList<Item>) StreamSupport.stream(iterable.spliterator(), false)
					    .collect(Collectors.toList());
			
		}
		CustomerOrderRequest customerOrderRequest = new CustomerOrderRequest(requestNumber, customerId, customerName, 
				phoneNumber, latLocation, longLocation, address, date, items);
		emailService.get(customerOrderRequest);
		return null;
	}

}*/
