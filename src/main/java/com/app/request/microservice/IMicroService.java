package com.app.request.microservice;

@org.springframework.stereotype.Service
public interface IMicroService {
	
	boolean isCustomerAvailable(String customerId);

	boolean isValidCartItem(String bhangarType, String bhangarUnit, Double bhangarPrice);

}