package com.app.request.microservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.app.request.entity.CustomerAvailableResponse;
import com.app.request.entity.ValidCartItemResponse;

@Service
public class MicroServiceImpl implements IMicroService {

	@Autowired
	RestTemplate rs;

	@Override
	public boolean isCustomerAvailable(String customerId){
		try {
			final MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			params.add("customerId", customerId);
			final CustomerAvailableResponse customerAvailableResponse = rs.exchange(
					UriComponentsBuilder.newInstance().scheme("https").host("web-thebhangarwale-login").pathSegment("login")
							.pathSegment("isCustomerAvailable").queryParams(params).build().toString(),
					HttpMethod.GET, null, CustomerAvailableResponse.class, params).getBody();
			return customerAvailableResponse.isCustomerAvailable();
		} catch (Exception e) {
			return false;
		}		
	}

	@Override
	public boolean isValidCartItem(String bhangarType, String bhangarUnit, Double bhangarPrice){
		try {
			final MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			params.add("bhangarType", bhangarType);
			params.add("bhangarUnit", bhangarUnit);
			params.add("bhangarPrice", String.valueOf(bhangarPrice));
			final ValidCartItemResponse validCartItemResponse = rs.exchange(
					UriComponentsBuilder.newInstance().scheme("https").host("web-thebhangarwale-admin").pathSegment("admin")
							.pathSegment("isValidCartItem").queryParams(params).build().toString(),
					HttpMethod.GET, null, ValidCartItemResponse.class, params).getBody();
			return validCartItemResponse.isValidCartItem();
		}catch (Exception e) {
			return false;
		}		
	}

}
