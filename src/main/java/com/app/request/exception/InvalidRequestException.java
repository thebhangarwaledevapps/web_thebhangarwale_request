package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidRequestException extends RuntimeException{
	
	public InvalidRequestException(String functionName,String parameter) {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}


}