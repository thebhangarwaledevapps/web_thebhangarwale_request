package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ItemOptionalIsNotPresentException extends Exception {
	
	public ItemOptionalIsNotPresentException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
