package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class CartItemOptionalNullException extends RuntimeException{
	
	public CartItemOptionalNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}


}
