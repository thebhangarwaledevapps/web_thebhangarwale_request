package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class UpdateItemQuantityException extends Exception {

	public UpdateItemQuantityException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
		
	}
	
}