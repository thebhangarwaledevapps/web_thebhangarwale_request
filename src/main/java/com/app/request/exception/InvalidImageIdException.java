package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidImageIdException extends RuntimeException{
	
	public InvalidImageIdException(String functionName) {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}