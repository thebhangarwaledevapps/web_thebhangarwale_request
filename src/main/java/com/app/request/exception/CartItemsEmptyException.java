package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class CartItemsEmptyException extends RuntimeException {
	
	public CartItemsEmptyException() {
		super(MessageUtils.PLEASE_ADD_ATLEAST_ONE_ITEM);
	}

}
