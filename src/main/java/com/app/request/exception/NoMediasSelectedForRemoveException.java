package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class NoMediasSelectedForRemoveException extends RuntimeException {

	public NoMediasSelectedForRemoveException() {
		super(MessageUtils.NO_MEDIAS_SELECTED);
	}
	
}
