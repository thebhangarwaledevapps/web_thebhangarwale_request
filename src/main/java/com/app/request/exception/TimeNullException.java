package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class TimeNullException extends RuntimeException {
	
	public TimeNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
