package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidRequestStatusException extends RuntimeException {
	
	public InvalidRequestStatusException() {
		super(MessageUtils.INVALID_REQUEST_STATUS);
	}

}
