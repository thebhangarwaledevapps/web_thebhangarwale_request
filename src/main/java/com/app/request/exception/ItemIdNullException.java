package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ItemIdNullException extends RuntimeException{
	
	public ItemIdNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	

}
