package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ItemNullException extends Exception {
	
	public ItemNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
