package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ItemListNullException extends Exception {

	public ItemListNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	
	

}
