package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class CartItemsNullException extends RuntimeException{
	
	public CartItemsNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
