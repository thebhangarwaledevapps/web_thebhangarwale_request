package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class RollbackException extends RuntimeException{
	
	public RollbackException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
