package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class OrderContentNotComposeException extends RuntimeException {
	
	public OrderContentNotComposeException() {
		super(MessageUtils.MAIL_NOT_SEND_SUCCESSFULLY);
	}

}
