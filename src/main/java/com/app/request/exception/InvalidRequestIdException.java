package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidRequestIdException extends RuntimeException {
	
	public InvalidRequestIdException() {
		super(MessageUtils.INVALID_REQUEST_ID);
	}

}
