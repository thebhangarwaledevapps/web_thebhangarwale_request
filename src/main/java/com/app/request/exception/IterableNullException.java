package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class IterableNullException extends RuntimeException {

	public IterableNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	
}
