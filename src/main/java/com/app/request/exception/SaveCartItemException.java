package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class SaveCartItemException extends RuntimeException {

	public SaveCartItemException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	
}
