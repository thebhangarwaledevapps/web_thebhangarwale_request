package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class SaveRequestException extends RuntimeException {
	
	public SaveRequestException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}


}
