package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class NoItemsSelectedForRemoveException extends RuntimeException{
	
	public NoItemsSelectedForRemoveException() {
		super(MessageUtils.NO_ITEMS_SELECTED);
	}


}
