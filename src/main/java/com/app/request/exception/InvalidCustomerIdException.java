package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidCustomerIdException extends RuntimeException{
	
	public InvalidCustomerIdException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}