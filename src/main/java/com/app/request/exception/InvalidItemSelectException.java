package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidItemSelectException extends RuntimeException{
	
	public InvalidItemSelectException() {
		super(MessageUtils.PLEASE_SELECT_VALID_ITEM);
	}


}