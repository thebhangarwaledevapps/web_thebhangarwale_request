package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidCustomerIdByAdminException extends RuntimeException {
	
	public InvalidCustomerIdByAdminException() {
		super(MessageUtils.INVALID_CUSTOMER_ID);
	}

}
