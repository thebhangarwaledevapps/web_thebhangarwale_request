package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ImageOutOfRangeException extends RuntimeException{
	
	public ImageOutOfRangeException() {
		super(MessageUtils.VALID_IMAGE_QUANTITY);
	}

}
