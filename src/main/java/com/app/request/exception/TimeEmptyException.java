package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class TimeEmptyException extends RuntimeException {
	
	public TimeEmptyException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
