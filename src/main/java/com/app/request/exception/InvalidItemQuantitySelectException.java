package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidItemQuantitySelectException extends RuntimeException{

	public InvalidItemQuantitySelectException() {
		super(MessageUtils.VALID_QUANTITY);
	}
	
}
