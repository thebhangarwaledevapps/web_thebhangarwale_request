package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class RequestNullException extends RuntimeException {
	
	public RequestNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
