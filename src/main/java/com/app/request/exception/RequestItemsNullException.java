package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class RequestItemsNullException extends RuntimeException {
	
	public RequestItemsNullException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
