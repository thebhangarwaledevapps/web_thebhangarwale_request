package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class SaveItemException extends RuntimeException {

	public SaveItemException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	
}
