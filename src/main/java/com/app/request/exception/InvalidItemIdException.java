package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidItemIdException extends RuntimeException{
	
	public InvalidItemIdException(String functionName) {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}