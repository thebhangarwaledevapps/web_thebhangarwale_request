package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ItemOptionalException extends RuntimeException{

	public ItemOptionalException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	
}
