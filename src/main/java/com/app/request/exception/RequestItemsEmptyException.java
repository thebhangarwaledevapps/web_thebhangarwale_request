package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class RequestItemsEmptyException extends RuntimeException {
	
	public RequestItemsEmptyException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
