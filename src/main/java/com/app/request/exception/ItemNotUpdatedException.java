package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class ItemNotUpdatedException extends RuntimeException {

	public ItemNotUpdatedException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
	
}
