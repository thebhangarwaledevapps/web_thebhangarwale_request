package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidBhangarwalePageRequestException extends RuntimeException {

	public InvalidBhangarwalePageRequestException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}
}
