package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidAddressIdException extends RuntimeException {
	
	public InvalidAddressIdException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
