package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class DateEmptyException extends RuntimeException {
	
	public DateEmptyException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
