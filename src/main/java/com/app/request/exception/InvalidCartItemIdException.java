package com.app.request.exception;

import com.app.request.utils.MessageUtils;

public class InvalidCartItemIdException extends RuntimeException {
	
	public InvalidCartItemIdException() {
		super(MessageUtils.SOMETHING_WENT_WRONG);
	}

}
