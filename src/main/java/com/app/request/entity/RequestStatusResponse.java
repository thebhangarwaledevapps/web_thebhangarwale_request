package com.app.request.entity;

public class RequestStatusResponse extends Response {

	private BhangarwaleRequest bhangarwaleRequest;

	public RequestStatusResponse(String message, BhangarwaleRequest bhangarwaleRequest) {
		super(message);
		this.bhangarwaleRequest = bhangarwaleRequest;
	}

	public BhangarwaleRequest getBhangarwaleRequest() {
		return bhangarwaleRequest;
	}

	public void setBhangarwaleRequest(BhangarwaleRequest bhangarwaleRequest) {
		this.bhangarwaleRequest = bhangarwaleRequest;
	}
	
}
