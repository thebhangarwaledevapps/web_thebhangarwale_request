package com.app.request.entity;


public class ItemResponse extends Response {

	private Item item;
	
	public ItemResponse(String message) {
		super(message);
	}

	public ItemResponse(String message, Item item) {
		super(message);
		this.item = item;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
}
