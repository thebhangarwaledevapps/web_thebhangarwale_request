package com.app.request.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RequestItem implements Serializable {
	
	@Id
	private Long itemId;

	public RequestItem() {
		super();
	}

	public RequestItem(Long itemId) {
		super();
		this.itemId = itemId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "RequestItem [itemId=" + itemId + "]";
	}
	
}
