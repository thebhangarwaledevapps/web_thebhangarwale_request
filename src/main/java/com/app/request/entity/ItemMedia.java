package com.app.request.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class ItemMedia implements Serializable {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long itemId;
	@NotBlank
	private String itemMediaPath;
	@NotBlank
	private String mimeType;

	public ItemMedia() {
	}

	public ItemMedia(@NotBlank String itemMediaPath, @NotBlank String mimeType) {
		super();
		this.itemMediaPath = itemMediaPath;
		this.mimeType = mimeType;
	}

	public long getItemId() {
		return itemId;
	}

	public String getItemMediaPath() {
		return itemMediaPath;
	}

	public void setItemMediaPath(String itemMediaPath) {
		this.itemMediaPath = itemMediaPath;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public String toString() {
		return "ItemMedia [itemId=" + itemId + ", itemMediaPath=" + itemMediaPath + ", mimeType=" + mimeType + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ItemMedia) {
			final ItemMedia itemMedia = (ItemMedia) obj;
			if (this.itemId == itemMedia.itemId && this.itemMediaPath.equals(itemMedia.itemMediaPath)
					&& this.mimeType.equals(itemMedia.mimeType)) {
				return true;
			}
		}
		return super.equals(obj);
	}

}
