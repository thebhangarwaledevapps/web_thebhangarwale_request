package com.app.request.entity;

import java.io.Serializable;

public class CustomerProfile implements Serializable{
	
	private Long customerId;
	private String firstName;
	private String lastName;
	
	public CustomerProfile() {
		super();
	}

	public CustomerProfile(Long customerId, String firstName, String lastName) {
		super();
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "CustomerProfile [customerId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ "]";
	}
	
}
