package com.app.request.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class DeleteItemRequest implements Serializable {
	
	private String customerId;
	private ArrayList<Item> items;
	
	public DeleteItemRequest(String customerId, ArrayList<Item> items) {
		super();
		this.customerId = customerId;
		this.items = items;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	
}
