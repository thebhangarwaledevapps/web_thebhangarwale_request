package com.app.request.entity;

import java.util.List;

public class MediasResponse  extends Response{

	private List<ItemMedia> itemMedias;
	
	public MediasResponse(String message) {
		super(message);
	}

	public MediasResponse(List<ItemMedia> itemMedias,String message) {
		super(message);
		this.itemMedias = itemMedias;
	}

	public List<ItemMedia> getItemMedias() {
		return itemMedias;
	}

	public void setItemMedias(List<ItemMedia> itemMedias) {
		this.itemMedias = itemMedias;
	}
	
}
