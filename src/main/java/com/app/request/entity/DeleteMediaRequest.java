package com.app.request.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class DeleteMediaRequest implements Serializable {
	
	private String customerId;
	private Long itemId;
	private ArrayList<ItemMedia> itemMedias;
	
	public DeleteMediaRequest(String customerId, Long itemId, ArrayList<ItemMedia> itemMedias) {
		super();
		this.customerId = customerId;
		this.itemId = itemId;
		this.itemMedias = itemMedias;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public ArrayList<ItemMedia> getItemMedias() {
		return itemMedias;
	}

	public void setItemMedias(ArrayList<ItemMedia> itemMedias) {
		this.itemMedias = itemMedias;
	}
	
}
