package com.app.request.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class CartItem implements Serializable {

	@Id
	private Long itemId;
	@NotNull
	private String customerId;
	
	public CartItem() {}

	public CartItem(Long itemId, @NotNull String customerId) {
		this.itemId = itemId;
		this.customerId = customerId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "CartItem [itemId=" + itemId + ", customerId=" + customerId + "]";
	}
	
}
