package com.app.request.entity;

import org.springframework.web.multipart.MultipartFile;

public class Image extends Media{

	private final MultipartFile multipartFile;

	public Image(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

}
