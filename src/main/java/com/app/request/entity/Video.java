package com.app.request.entity;

import org.springframework.web.multipart.MultipartFile;

public class Video extends Media{

	private final MultipartFile multipartFile;

	public Video(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

}
