package com.app.request.entity;

import java.util.ArrayList;

public class CartItemResponse extends Response{

	private ArrayList<Item> items;
	private Integer minimumWeight;
	
	public CartItemResponse(String message) {
		super(message);
	}

	public CartItemResponse(String message, Integer minimumWeight) {
		super(message);
		this.minimumWeight = minimumWeight;
	}

	public CartItemResponse(String message, ArrayList<Item> items, Integer minimumWeight) {
		super(message);
		this.items = items;
		this.minimumWeight = minimumWeight;
	}
	
	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	
}
