package com.app.request.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class Item implements Serializable{
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long itemId;
	@NotBlank
	private String itemName;
	private int itemQuantity;
	private double itemPrice;
	private double totalItemPriceForUserAsPerQuantity;
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval = true)
	private List<ItemMedia> medias;
	
	public Item() {}

	public Item(@NotBlank String itemName, int itemQuantity, double itemPrice,
			double totalItemPriceForUserAsPerQuantity, List<ItemMedia> medias) {
		super();
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		this.itemPrice = itemPrice;
		this.totalItemPriceForUserAsPerQuantity = totalItemPriceForUserAsPerQuantity;
		this.medias = medias;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public double getTotalItemPriceForUserAsPerQuantity() {
		return totalItemPriceForUserAsPerQuantity;
	}

	public void setTotalItemPriceForUserAsPerQuantity(double totalItemPriceForUserAsPerQuantity) {
		this.totalItemPriceForUserAsPerQuantity = totalItemPriceForUserAsPerQuantity;
	}

	public List<ItemMedia> getMedias() {
		return medias;
	}

	public void setMedias(List<ItemMedia> medias) {
		this.medias = medias;
	}

}
