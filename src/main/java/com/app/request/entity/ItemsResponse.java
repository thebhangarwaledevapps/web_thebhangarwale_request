package com.app.request.entity;

import java.util.ArrayList;

public class ItemsResponse extends Response{

	private ArrayList<Item> items;
	private String totalAmount;
	private String totalWeight;
	
	public ItemsResponse(String message) {
		super(message);
	}

	public ItemsResponse(ArrayList<Item> items, String totalAmount, String totalWeight) {
		super(null);
		this.items = items;
		this.totalAmount = totalAmount;
		this.totalWeight = totalWeight;
	}
	
	public ItemsResponse(ArrayList<Item> items, String totalAmount, String totalWeight,String message) {
		super(message);
		this.items = items;
		this.totalAmount = totalAmount;
		this.totalWeight = totalWeight;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

}
