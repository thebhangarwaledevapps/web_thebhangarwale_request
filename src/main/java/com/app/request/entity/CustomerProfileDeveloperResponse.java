package com.app.request.entity;

public class CustomerProfileDeveloperResponse extends Response{

	private CustomerProfile customerProfile;
	
	public CustomerProfileDeveloperResponse(String message) {
		super(message);
	}

	public CustomerProfileDeveloperResponse(String message, CustomerProfile customerProfile) {
		super(message);
		this.customerProfile = customerProfile;
	}

	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

}
