package com.app.request.entity;

import java.io.Serializable;

public class ValidCartItemResponse implements Serializable{

	private boolean isValidCartItem;
	private String message;
	
	public ValidCartItemResponse() {}

	public ValidCartItemResponse(boolean isValidCartItem) {
		this.isValidCartItem = isValidCartItem;
	}

	public ValidCartItemResponse(String message) {
		this.message = message;
	}

	public ValidCartItemResponse(boolean isValidCartItem, String message) {
		this.isValidCartItem = isValidCartItem;
		this.message = message;
	}

	public boolean isValidCartItem() {
		return isValidCartItem;
	}

	public void setValidCartItem(boolean isValidCartItem) {
		this.isValidCartItem = isValidCartItem;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
