package com.app.request.entity;

import java.util.ArrayList;

public class RequestResponse extends Response{
	
	private ArrayList<BhangarwaleRequest> requests;
	private Integer minimumWeight;
	
	public RequestResponse(String message) {
		super(message);
	}

	public RequestResponse(String message, ArrayList<BhangarwaleRequest> requests) {
		super(message);
		this.requests = requests;
	}
	
	public RequestResponse(String message, Integer minimumWeight) {
		super(message);
		this.minimumWeight = minimumWeight;
	}

	public RequestResponse(String message, ArrayList<BhangarwaleRequest> requests, Integer minimumWeight) {
		super(message);
		this.requests = requests;
		this.minimumWeight = minimumWeight;
	}
	
	public ArrayList<BhangarwaleRequest> getRequests() {
		return requests;
	}

	public void setRequests(ArrayList<BhangarwaleRequest> requests) {
		this.requests = requests;
	}
		
}
