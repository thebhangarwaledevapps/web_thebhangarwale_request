package com.app.request.entity;

public class AddressForDeveloperRequest extends Request{
	
	private Long addressId;

	public AddressForDeveloperRequest(long customerId, Long addressId) {
		super(customerId);
		this.addressId = addressId;
	}

	public Long getAddressId() {
		return addressId;
	}

}
