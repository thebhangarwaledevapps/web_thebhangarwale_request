package com.app.request.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class BhangarwaleRequest implements Serializable{

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long requestId;
	private Long customerId;
	private Long addressId;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "request_Id")
	private List<RequestItem> requestItems;
	@Enumerated(EnumType.STRING)
    private RequestStatus status;
	private String createdDate;
	private String createdTime;
	
	public BhangarwaleRequest() {
		super();
	}

	public BhangarwaleRequest(Long customerId, Long addressId, List<RequestItem> requestItems, RequestStatus status,
			String createdDate, String createdTime) {
		super();
		this.customerId = customerId;
		this.addressId = addressId;
		this.requestItems = requestItems;
		this.status = status;
		this.createdDate = createdDate;
		this.createdTime = createdTime;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public List<RequestItem> getRequestItems() {
		return requestItems;
	}

	public void setRequestItems(List<RequestItem> requestItems) {
		this.requestItems = requestItems;
	}

	public RequestStatus getStatus() {
		return status;
	}

	public void setStatus(RequestStatus status) {
		this.status = status;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	@Override
	public String toString() {
		return "Request [requestId=" + requestId + ", customerId=" + customerId + ", addressId=" + addressId
				+ ", requestItems=" + requestItems + ", status=" + status + ", createdDate=" + createdDate
				+ ", createdTime=" + createdTime + "]";
	}
	
}
