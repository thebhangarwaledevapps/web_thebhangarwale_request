package com.app.request.entity;

import java.io.Serializable;

public class Customer implements Serializable{

	private Long customerId;
	private String phoneNumber;
	private String name;
	
	public Customer() {
		super();
	}
	
	public Customer(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
	}

	public Customer(String phoneNumber, String name) {
		super();
		this.phoneNumber = phoneNumber;
		this.name = name;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getName() {
		return name;
	}

}
