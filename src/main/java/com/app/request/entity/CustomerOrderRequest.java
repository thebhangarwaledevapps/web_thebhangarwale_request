package com.app.request.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomerOrderRequest implements Serializable{
	
	private Long requestNumber;
	private Long customerId;
	private String customerName;
	private String phoneNumber;
	private double latLocation;
	private double longLocation;
	private String address;
	private String date;
	private ArrayList<Item> items;
	
	public CustomerOrderRequest(Long requestNumber, Long customerId, String customerName, String phoneNumber,
			double latLocation, double longLocation, String address, String date,ArrayList<Item> items) {
		super();
		this.requestNumber = requestNumber;
		this.customerId = customerId;
		this.customerName = customerName;
		this.phoneNumber = phoneNumber;
		this.latLocation = latLocation;
		this.longLocation = longLocation;
		this.address = address;
		this.date = date;
		this.items = items;
	}

	/**
	 * @return the orderId
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the latLocation
	 */
	public double getLatLocation() {
		return latLocation;
	}

	/**
	 * @param latLocation the latLocation to set
	 */
	public void setLatLocation(double latLocation) {
		this.latLocation = latLocation;
	}

	/**
	 * @return the longLocation
	 */
	public double getLongLocation() {
		return longLocation;
	}

	/**
	 * @param longLocation the longLocation to set
	 */
	public void setLongLocation(double longLocation) {
		this.longLocation = longLocation;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the items
	 */
	public ArrayList<Item> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	
	
	
}
