package com.app.request.entity;

import java.io.Serializable;

public class Address implements Serializable{

	private Long addressId;
	private Long customerId;
	private String address;
	private String pincode;
	private String landMark;
	private double latLocation;
	private double longLocation;
	
	public Address() {
		super();
	}

	public Address(Long customerId, String address, String pincode, String landMark, double latLocation,
			double longLocation) {
		super();
		this.customerId = customerId;
		this.address = address;
		this.pincode = pincode;
		this.landMark = landMark;
		this.latLocation = latLocation;
		this.longLocation = longLocation;
	}
	
	public Address(Long addressId, Long customerId, String address, String pincode, String landMark, double latLocation,
			double longLocation) {
		super();
		this.addressId = addressId;
		this.customerId = customerId;
		this.address = address;
		this.pincode = pincode;
		this.landMark = landMark;
		this.latLocation = latLocation;
		this.longLocation = longLocation;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getLandMark() {
		return landMark;
	}

	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}

	public double getLatLocation() {
		return latLocation;
	}

	public void setLatLocation(double latLocation) {
		this.latLocation = latLocation;
	}

	public double getLongLocation() {
		return longLocation;
	}

	public void setLongLocation(double longLocation) {
		this.longLocation = longLocation;
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", customerId=" + customerId + ", address=" + address + ", pincode="
				+ pincode + ", landMark=" + landMark + ", latLocation=" + latLocation + ", longLocation=" + longLocation
				+ "]";
	}
	
	
	
	
		
}

