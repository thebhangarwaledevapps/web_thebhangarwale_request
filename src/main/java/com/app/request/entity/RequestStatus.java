package com.app.request.entity;

public enum RequestStatus {
	CREATED,CONFIRMED,CANCELLED,COMPLETED
}
