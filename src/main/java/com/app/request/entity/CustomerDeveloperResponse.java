package com.app.request.entity;

public class CustomerDeveloperResponse extends Response{

	private Customer customer;

	public CustomerDeveloperResponse() {
		super("");
	}

	public CustomerDeveloperResponse(String message) {
		super(message);
	}

	public CustomerDeveloperResponse(String message, Customer customer) {
		super(message);
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "CustomerDeveloperResponse [customer=" + customer + ",message =" +message+"]";
	}
	
}
