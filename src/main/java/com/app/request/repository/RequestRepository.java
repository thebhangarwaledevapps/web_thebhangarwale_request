package com.app.request.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.request.entity.BhangarwaleRequest;

public interface RequestRepository extends CrudRepository<BhangarwaleRequest, Long>{
	
	@Query("FROM BhangarwaleRequest WHERE customerId =:customerId AND requestId =:requestId")
	Optional<BhangarwaleRequest> findBhangarwaleRequestByCustomerIdAndRequestId(@Param("customerId")Long customerId,@Param("requestId")Long requestId);
	
	@Query("FROM BhangarwaleRequest WHERE customerId = ?1")
	Optional<ArrayList<BhangarwaleRequest>> findBhangarwaleRequestByCustomerId(@Param("customerId")Long customerId);
		
}
