package com.app.request.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import com.app.request.entity.ItemMedia;

public interface ItemMediaRepository extends JpaRepository<ItemMedia, Long>{}
