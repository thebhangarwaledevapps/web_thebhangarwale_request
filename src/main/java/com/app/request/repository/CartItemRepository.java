package com.app.request.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.request.entity.CartItem;

public interface CartItemRepository extends CrudRepository<CartItem, Long>{

	@Query("FROM CartItem WHERE customerId = ?1")
	public Optional<ArrayList<CartItem>> findCartItemsByCustomerId(String customerId);
	
	
	@Query("FROM CartItem WHERE customerId =:customerId AND itemId =:itemId")
	Optional<CartItem> findCartItemByCustomerIdAndItemId(@Param("customerId")String customerId,@Param("itemId")Long itemId);
	
	
	
}
