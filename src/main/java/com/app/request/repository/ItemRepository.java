package com.app.request.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.app.request.entity.Item;

public interface ItemRepository extends JpaRepository<Item, Long>{
	
	@Query(nativeQuery=true, value="SELECT SUM(item_quantity) FROM Item, cart_item WHERE Item.item_id = cart_item.item_id AND customer_id =:customer_id")
	public Long getItemQuantity(@Param("customer_id")Long customerId);
		
}
