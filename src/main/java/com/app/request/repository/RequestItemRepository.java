package com.app.request.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.app.request.entity.RequestItem;

public interface RequestItemRepository extends CrudRepository<RequestItem, Long>{
	
	@Query("FROM RequestItem WHERE request_Id = ?1")
	Optional<ArrayList<RequestItem>> findRequestByItemId(Long orderId);
	
}