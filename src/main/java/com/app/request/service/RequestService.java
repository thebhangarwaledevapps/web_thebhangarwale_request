package com.app.request.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.app.request.entity.CartItem;
import com.app.request.entity.CartItemResponse;
import com.app.request.entity.CustomerAvailableResponse;
import com.app.request.entity.Item;
import com.app.request.entity.ItemMedia;
import com.app.request.entity.ItemResponse;
import com.app.request.entity.ItemsResponse;
import com.app.request.entity.MediasResponse;
import com.app.request.entity.BhangarwaleRequest;
import com.app.request.entity.RequestItem;
import com.app.request.entity.RequestResponse;
import com.app.request.entity.RequestStatus;
import com.app.request.entity.RequestStatusResponse;
import com.app.request.entity.Response;
import com.app.request.exception.ImageOutOfRangeException;
import com.app.request.exception.InvalidAddressIdException;
import com.app.request.exception.InvalidCartItemIdException;
import com.app.request.exception.InvalidCustomerIdByAdminException;
import com.app.request.exception.CartItemOptionalNullException;
import com.app.request.exception.CartItemsEmptyException;
import com.app.request.exception.CartItemsNullException;
import com.app.request.exception.DateEmptyException;
import com.app.request.exception.InvalidCustomerIdException;
import com.app.request.exception.InvalidImageIdException;
import com.app.request.exception.ItemOptionalException;
import com.app.request.exception.ItemOptionalIsNotPresentException;
import com.app.request.exception.IterableNullException;
import com.app.request.exception.NoItemsSelectedForRemoveException;
import com.app.request.exception.NoMediasSelectedForRemoveException;
import com.app.request.exception.RequestItemsEmptyException;
import com.app.request.exception.RequestItemsNullException;
import com.app.request.exception.RequestNullException;
import com.app.request.exception.RollbackException;
import com.app.request.exception.InvalidItemIdException;
import com.app.request.exception.InvalidItemQuantitySelectException;
import com.app.request.exception.InvalidItemSelectException;
import com.app.request.exception.InvalidRequestIdException;
import com.app.request.exception.InvalidRequestStatusException;
import com.app.request.exception.ItemIdNullException;
import com.app.request.exception.ItemListNullException;
import com.app.request.exception.ItemNotUpdatedException;
import com.app.request.exception.ItemNullException;
import com.app.request.exception.SaveCartItemException;
import com.app.request.exception.SaveItemException;
import com.app.request.exception.SaveRequestException;
import com.app.request.exception.TimeEmptyException;
import com.app.request.exception.TimeNullException;
import com.app.request.exception.UpdateItemQuantityException;
import com.app.request.microservice.IMicroService;
import com.app.request.repository.CartItemRepository;
import com.app.request.repository.ItemMediaRepository;
import com.app.request.repository.ItemRepository;
import com.app.request.repository.RequestRepository;
import com.app.request.utils.MessageUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
@Transactional
public class RequestService {

	@Autowired
	ItemMediaRepository itemMediaRepository;

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	CartItemRepository cartItemRepository;

	@Autowired
	RequestRepository requestRepository;
	
	@Autowired
	IMicroService microServiceImpl;
	
	@Autowired
	com.app.request.utils.Service utilService;
	
	public Response addItemToCart(String customerId,String bhangarType,String bhangarUnit,Double bhangarPrice,int itemQuantityByUser,MultipartFile[] mediaList)
			throws InvalidItemSelectException, InvalidItemQuantitySelectException, ImageOutOfRangeException, Exception {
		if (bhangarType == null) {
			throw new InvalidItemSelectException();
		}
		if (bhangarType.trim().isEmpty()) {
			throw new InvalidItemSelectException();
		}
		if(!microServiceImpl.isValidCartItem(bhangarType,bhangarUnit,bhangarPrice)) {
			throw new InvalidItemSelectException();
		}
		if (itemQuantityByUser <= 0) {
			throw new InvalidItemQuantitySelectException();
		}
		if (mediaList != null && mediaList.length > 3) {
			throw new ImageOutOfRangeException();
		}
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		ArrayList<ItemMedia> mediaArrayList = utilService.saveMedia(mediaList);	
		double totalItemPrice = bhangarPrice * itemQuantityByUser;
		final Item item = itemRepository.save(new Item(bhangarType, itemQuantityByUser,bhangarPrice,totalItemPrice, mediaArrayList));
		cartItemRepository.save(new CartItem(item.getItemId(), customerId));
		return new Response(MessageUtils.ITEM_SAVE_SUCCESSFULLY) {};		
	}
	
	public ItemsResponse getItemsInCart(String customerId) throws CartItemsEmptyException, Exception{
		final Optional<ArrayList<CartItem>> cartItemsOptional = cartItemRepository.findCartItemsByCustomerId(customerId);
		if(cartItemsOptional.isPresent()) {
			ArrayList<Long> itemIds = (ArrayList<Long>) cartItemsOptional.get().stream().map(new Function<CartItem, Long>() {
				@Override
				public Long apply(CartItem t) {
					return t.getItemId();
				}
			}).collect(Collectors.toList());	
			if(itemIds!=null && itemIds.isEmpty()) {
				throw new CartItemsEmptyException();
			}
			ArrayList<Item> items = (ArrayList<Item>) itemRepository.findAllById(itemIds);
			final String totalAmount = String.valueOf(items.stream().mapToDouble(new ToDoubleFunction<Item>() {
				@Override
				public double applyAsDouble(Item value) {
					return value.getTotalItemPriceForUserAsPerQuantity();
				}
			}).sum());
			final String totalWeight = String.valueOf(items.stream().mapToDouble(new ToDoubleFunction<Item>() {
				@Override
				public double applyAsDouble(Item value) {
					return value.getItemQuantity();
				}
			}).sum());
			return new ItemsResponse(items,totalAmount,totalWeight);
		}
		throw new CartItemsEmptyException();
	}

	public ItemsResponse deleteItemInCart(String customerId, ArrayList<Item> items) throws NoItemsSelectedForRemoveException,Exception {
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		if (items == null || items.isEmpty()) {
			throw new NoItemsSelectedForRemoveException();
		}
		itemRepository.deleteAll(items);
		List<CartItem> cartItemlist = items.stream().map(item -> new CartItem(item.getItemId(), customerId))
				.collect(Collectors.toList());
		cartItemRepository.deleteAll(cartItemlist);
		Optional<ArrayList<CartItem>> cartItemsOptional = cartItemRepository.findCartItemsByCustomerId(customerId);
		ArrayList<CartItem> cartItems = cartItemsOptional.get();
		List<Long> itemIds = cartItems.stream().map(cartItem -> cartItem.getItemId()).collect(Collectors.toList());
		ArrayList<Item> itemList = new ArrayList<Item>((Collection<? extends Item>) itemRepository.findAllById(itemIds));
		final String totalAmount = String.valueOf(itemList.stream().mapToDouble(new ToDoubleFunction<Item>() {
			@Override
			public double applyAsDouble(Item value) {
				return value.getTotalItemPriceForUserAsPerQuantity();
			}
		}).sum());
		final String totalWeight = String.valueOf(itemList.stream().mapToDouble(new ToDoubleFunction<Item>() {
			@Override
			public double applyAsDouble(Item value) {
				return value.getItemQuantity();
			}
		}).sum());
		return new ItemsResponse(itemList,totalAmount,totalWeight,MessageUtils.ITEM_REMOVE_SUCCESSFULLY);
	}
	
	public Response updateItemInCart(String customerId, Long itemId, Integer itemQuantityByUser,MultipartFile[] mediaList) throws InvalidItemQuantitySelectException,ImageOutOfRangeException,Exception{
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		final Optional<CartItem> cartItemOptional = cartItemRepository.findCartItemByCustomerIdAndItemId(customerId, itemId);
		if (cartItemOptional == null || !cartItemOptional.isPresent()) {
			throw new Exception();
		}
		final Optional<Item> itemOptional = itemRepository.findById(itemId);
		if (itemOptional == null) {
			throw new Exception();
		}
		if(!itemOptional.isPresent()) {
			throw new Exception();
		}
		final Item item = itemOptional.get();
		if (itemQuantityByUser!=null && itemQuantityByUser <= 0) {
			throw new InvalidItemQuantitySelectException();
		}
		if(item.getMedias()!=null && mediaList!=null) {
			if((item.getMedias().size() + mediaList.length) > 3) {
				throw new ImageOutOfRangeException();
			}
		}
		if(itemQuantityByUser!=null) {
			item.setItemQuantity(itemQuantityByUser);
			double totalItemPrice = item.getItemPrice() * itemQuantityByUser;
			item.setTotalItemPriceForUserAsPerQuantity(totalItemPrice);	
		}
		ArrayList<ItemMedia> mediaArrayList = utilService.saveMedia(mediaList);	
		if(mediaArrayList!=null) {
			item.getMedias().addAll(mediaArrayList);
		}
		itemRepository.save(item);
		return new Response(MessageUtils.ITEM_UPDATE_SUCCESSFULLY) {};
	}
	
	public MediasResponse deleteImagesFromItem(String customerId, Long itemId, ArrayList<ItemMedia> medias) throws NoMediasSelectedForRemoveException,Exception {
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		final Optional itemOptional = itemRepository.findById(itemId);
		if(itemOptional==null || !itemOptional.isPresent()) {
			throw new Exception();	
		}
		if (medias == null || medias.isEmpty()) {
			throw new NoMediasSelectedForRemoveException();
		}
		final Item item = (Item) itemOptional.get();
		item.getMedias().removeAll(medias);
		itemRepository.save(item);
		return new MediasResponse(itemRepository.findById(itemId).get().getMedias(),MessageUtils.MEDIA_REMOVE_SUCCESSFULLY);
	}

	/*public Response createOrder(Long customerId, Long addressId)
			throws InvalidCustomerIdException, InvalidAddressIdException, CartItemOptionalNullException,
			CartItemOptionalIsNotPresentException, CartItemsNullException, CartItemsEmptyException,
			InvalidCartItemIdException, RequestItemsNullException, RequestItemsEmptyException, DateNullException,
			DateEmptyException, TimeNullException, SaveRequestException, Exception {
		if (customerId == null) {
			throw new InvalidCustomerIdException("");
		}
		if (addressId == null) {
			throw new InvalidAddressIdException();
		}
		Optional<ArrayList<CartItem>> cartItemOptional = cartItemRepository.findCartItemsByCustomerId(customerId);
		if (cartItemOptional == null) {
			throw new CartItemOptionalNullException();
		}
		if (!cartItemOptional.isPresent()) {
			throw new CartItemOptionalIsNotPresentException();
		}
		ArrayList<CartItem> cartItems = cartItemOptional.get();
		if (cartItems == null) {
 			throw new CartItemsNullException();
		}
		if (cartItems.isEmpty()) {
			throw new CartItemsEmptyException();
		}
		Boolean isAnyCartItemIdNull = cartItems.stream().anyMatch(cartitem -> cartitem.getItemId() == null);
		if (isAnyCartItemIdNull) {
			throw new InvalidCartItemIdException();
		}

		ArrayList<RequestItem> requestItems = (ArrayList<RequestItem>) cartItems.stream()
				.map(cartitem -> new RequestItem(cartitem.getItemId())).collect(Collectors.toList());
		if (requestItems == null) {
			throw new RequestItemsNullException();
		}
		if (requestItems.isEmpty()) {
			throw new RequestItemsEmptyException();
		}
		String createdDate = DateUtils.getCurrentDate();
		if (createdDate == null) {
			throw new DateNullException();
		}
		if (createdDate.trim().isEmpty()) {
			throw new DateEmptyException();
		}
		String createdTime = TimeUtils.getCurrentTime();
		if (createdTime == null) {
			throw new TimeNullException();
		}
		if (createdTime.trim().isEmpty()) {
			throw new TimeEmptyException();
		}
		BhangarwaleRequest request = new BhangarwaleRequest(customerId, addressId, requestItems, RequestStatus.CREATED,
				createdDate, createdTime);
		BhangarwaleRequest requestObj = requestRepository.save(request);
		if (requestObj == null) {
			throw new SaveRequestException();
		}
		if (requestObj.getRequestId() == null) {
			throw new SaveRequestException();
		}
		Async<BhangarwaleRequest, Void> asyncFunction = new Async<BhangarwaleRequest, Void>(sendEmailService);
		asyncFunction.apply(requestObj);
		return new Response(MessageUtils.ORDER_CREATED_SUCCESSFULLY) {
		};
	}

	public RequestResponse getRequests(Long customerId)
			throws InvalidCustomerIdException, IterableNullException, RequestNullException, Exception {
		if (customerId == null) {
			throw new InvalidCustomerIdException("");
		}
		Optional<ArrayList<BhangarwaleRequest>> bhangarwaleRequestArrayListOptional = requestRepository
				.findBhangarwaleRequestByCustomerId(customerId);
		if (bhangarwaleRequestArrayListOptional == null) {
			throw new BhangarwaleRequestArrayListOptionalIsNullException();
		}
		if (!bhangarwaleRequestArrayListOptional.isPresent()) {
			throw new BhangarwaleRequestOptionalArrayListIsNotPresentException();
		}
		Integer minimumWeight = (Integer) adminMinimumWeightService.get();
		ArrayList<BhangarwaleRequest> bhangarwaleRequestArrayList = bhangarwaleRequestArrayListOptional.get();
		if (bhangarwaleRequestArrayList == null) {
			throw new BhangarwaleArrayListNullException();
		}
		if (bhangarwaleRequestArrayList.isEmpty()) {
			return new RequestResponse(MessageUtils.NO_REQUEST_FOUND, minimumWeight);
		}
		return new RequestResponse(null, bhangarwaleRequestArrayList);
	}

	public RequestResponse cancelRequest(Long customerId, Long requestId)
			throws InvalidCustomerIdException, InvalidRequestIdException, BhangarwaleRequestOptionalIsNullException,
			BhangarwaleRequestOptionalIsNotPresentException, BhangarwaleOptionalRequestNullException,
			InvalidRequestStatusException, BhangarwaleRequestObjectIsNullException, Exception {
		if (customerId == null) {
			throw new InvalidCustomerIdException("");
		}
		if (requestId == null) {
			throw new InvalidRequestIdException();
		}
		Optional<BhangarwaleRequest> bhangarWaleRequestOptional = requestRepository
				.findBhangarwaleRequestByCustomerIdAndRequestId(customerId, requestId);
		if (bhangarWaleRequestOptional == null) {
			throw new BhangarwaleRequestOptionalIsNullException();
		}
		if (!bhangarWaleRequestOptional.isPresent()) {
			throw new BhangarwaleRequestOptionalIsNotPresentException();
		}
		BhangarwaleRequest bhangarwaleRequest = bhangarWaleRequestOptional.get();
		if (bhangarwaleRequest == null) {
			throw new BhangarwaleOptionalRequestNullException();
		}
		bhangarwaleRequest.setStatus(RequestStatus.CANCELLED);
		BhangarwaleRequest bhangarwaleRequestObj = requestRepository.save(bhangarwaleRequest);
		if (bhangarwaleRequestObj == null) {
			throw new BhangarwaleRequestObjectIsNullException();
		}
		Optional<ArrayList<BhangarwaleRequest>> bhangarwaleRequestArrayListOptional = requestRepository
				.findBhangarwaleRequestByCustomerId(customerId);
		if (bhangarwaleRequestArrayListOptional == null) {
			throw new BhangarwaleRequestArrayListOptionalIsNullException();
		}
		if (!bhangarwaleRequestArrayListOptional.isPresent()) {
			throw new BhangarwaleRequestOptionalArrayListIsNotPresentException();
		}
		ArrayList<BhangarwaleRequest> bhangarwaleRequestArrayList = bhangarwaleRequestArrayListOptional.get();
		if (bhangarwaleRequestArrayList == null) {
			throw new BhangarwaleArrayListNullException();
		}
		return new RequestResponse(MessageUtils.REQUEST_CANCELLED_SUCCESSFULLY, bhangarwaleRequestArrayList);
	}

	public RequestStatusResponse confirmRequestByAdmin(Long customerId, Long requestId)
			throws InvalidCustomerIdByAdminException, InvalidRequestIdException,
			BhangarwaleRequestOptionalIsNullException, BhangarwaleRequestOptionalIsNotPresentException,
			BhangarwaleOptionalRequestNullException, InvalidRequestStatusException,
			BhangarwaleRequestObjectIsNullException, Exception {

		if (customerId == null) {
			throw new InvalidCustomerIdByAdminException();
		}
		if (requestId == null) {
			throw new InvalidRequestIdException();
		}
		Optional<BhangarwaleRequest> bhangarWaleRequestOptional = requestRepository
				.findBhangarwaleRequestByCustomerIdAndRequestId(customerId, requestId);
		if (bhangarWaleRequestOptional == null) {
			throw new BhangarwaleRequestOptionalIsNullException();
		}
		if (!bhangarWaleRequestOptional.isPresent()) {
			throw new BhangarwaleRequestOptionalIsNotPresentException();
		}
		BhangarwaleRequest bhangarwaleRequest = bhangarWaleRequestOptional.get();
		if (bhangarwaleRequest == null) {
			throw new BhangarwaleOptionalRequestNullException();
		}
		bhangarwaleRequest.setStatus(RequestStatus.CONFIRMED);
		BhangarwaleRequest bhangarwaleRequestObj = requestRepository.save(bhangarwaleRequest);
		if (bhangarwaleRequestObj == null) {
			throw new BhangarwaleRequestObjectIsNullException();
		}
		return new RequestStatusResponse(MessageUtils.REQUEST_CONFIRMED_SUCCESSFULLY, bhangarwaleRequestObj);
	}

	public RequestStatusResponse cancelRequestByAdmin(Long customerId, Long requestId)
			throws InvalidCustomerIdByAdminException, InvalidRequestIdException,
			BhangarwaleRequestOptionalIsNullException, BhangarwaleRequestOptionalIsNotPresentException,
			BhangarwaleOptionalRequestNullException, InvalidRequestStatusException,
			BhangarwaleRequestObjectIsNullException, Exception {

		if (customerId == null) {
			throw new InvalidCustomerIdByAdminException();
		}
		if (requestId == null) {
			throw new InvalidRequestIdException();
		}
		Optional<BhangarwaleRequest> bhangarWaleRequestOptional = requestRepository
				.findBhangarwaleRequestByCustomerIdAndRequestId(customerId, requestId);
		if (bhangarWaleRequestOptional == null) {
			throw new BhangarwaleRequestOptionalIsNullException();
		}
		if (!bhangarWaleRequestOptional.isPresent()) {
			throw new BhangarwaleRequestOptionalIsNotPresentException();
		}
		BhangarwaleRequest bhangarwaleRequest = bhangarWaleRequestOptional.get();
		if (bhangarwaleRequest == null) {
			throw new BhangarwaleOptionalRequestNullException();
		}
		bhangarwaleRequest.setStatus(RequestStatus.CANCELLED);
		BhangarwaleRequest bhangarwaleRequestObj = requestRepository.save(bhangarwaleRequest);
		if (bhangarwaleRequestObj == null) {
			throw new BhangarwaleRequestObjectIsNullException();
		}
		return new RequestStatusResponse(MessageUtils.REQUEST_CANCELLED_SUCCESSFULLY, bhangarwaleRequestObj) {
		};
	}

	public RequestStatusResponse completeRequestByAdmin(Long customerId, Long requestId)
			throws InvalidCustomerIdByAdminException, InvalidRequestIdException,
			BhangarwaleRequestOptionalIsNullException, BhangarwaleRequestOptionalIsNotPresentException,
			BhangarwaleOptionalRequestNullException, InvalidRequestStatusException,
			BhangarwaleRequestObjectIsNullException, Exception {

		if (customerId == null) {
			throw new InvalidCustomerIdByAdminException();
		}
		if (requestId == null) {
			throw new InvalidRequestIdException();
		}
		Optional<BhangarwaleRequest> bhangarWaleRequestOptional = requestRepository
				.findBhangarwaleRequestByCustomerIdAndRequestId(customerId, requestId);
		if (bhangarWaleRequestOptional == null) {
			throw new BhangarwaleRequestOptionalIsNullException();
		}
		if (!bhangarWaleRequestOptional.isPresent()) {
			throw new BhangarwaleRequestOptionalIsNotPresentException();
		}
		BhangarwaleRequest bhangarwaleRequest = bhangarWaleRequestOptional.get();
		if (bhangarwaleRequest == null) {
			throw new BhangarwaleOptionalRequestNullException();
		}
		bhangarwaleRequest.setStatus(RequestStatus.COMPLETED);
		BhangarwaleRequest bhangarwaleRequestObj = requestRepository.save(bhangarwaleRequest);
		if (bhangarwaleRequestObj == null) {
			throw new BhangarwaleRequestObjectIsNullException();
		}
		return new RequestStatusResponse(MessageUtils.REQUEST_COMPLETED_SUCCESSFULLY, bhangarwaleRequestObj) {
		};

	}*/

}
